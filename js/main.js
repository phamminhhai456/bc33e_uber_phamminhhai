const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhGiaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
}

function tinhGiaTienKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
      //   break;
    }
    case UBER_SUV: {
      return 8500;
      //   break;
    }
    case UBER_BLACK: {
      return 9500;
      //   break;
    }
    default:
      return 0;
  }
}

function tinhGiaTienKm19TroDi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
      //   break;
    }
    case UBER_SUV: {
      return 8000;
      //   break;
    }
    case UBER_BLACK: {
      return 9000;
      //   break;
    }
    default:
      return 0;
  }
}

// main function
function tinhTienUber() {
  var carOption = document.querySelector(
    'input[name="selector"]:checked'
  ).value;

  // console.log("carOption: ", carOption);

  var soKm = document.getElementById("txt-km").value * 1;

  var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption) * 1;
  // console.log(giaTienKmDauTien);

  var giaTienKm1_19 = tinhGiaTienKm1_19(carOption) * 1;
  // console.log(giaTienKm1_19);

  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carOption) * 1;
  // console.log(giaTienKm19TroDi);

  var tongTien = 0;

  if (soKm <= 1) {
    tongTien = giaTienKmDauTien * soKm;
  } else if (soKm <= 19) {
    tongTien = giaTienKmDauTien + (soKm - 1) * giaTienKm1_19;
  } else {
    tongTien =
      giaTienKmDauTien + 18 * giaTienKm1_19 + (soKm - 19) * giaTienKm19TroDi;
  }
  // console.log(tongTien);

  document.getElementById("xuatTien").innerHTML = tongTien;

  document.getElementById("divThanhTien").style.display = "block";
}
